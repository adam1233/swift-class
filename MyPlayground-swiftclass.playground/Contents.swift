//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


import Foundation

// Initial variables (just to get you started)
var numberOfPlayers = 4
var board: [Int] = []

// Functions
func boardSetup() {
    var counter: Int = 0
    while counter < 100 {
        board.append (0)
        counter = counter + 1
    }
    // Boring board
    board
    
    // Fun board
    board[6] = 15
    board[9] = -12
    board[12] = -3
    board[16] = 3
}



func rollDice() ->Int{
    // Generate a random number between 1 to 6
    // Hint: use the rand() function to get a random number, then limit it to between 1 to 6
    let randomDice: Int = Int(arc4random_uniform(6))
    print("Dice random rolled: \(randomDice)")
    
    return randomDice
}

// Pass it a player number, rolled dice, etc
// and print out a description of the move
func printStep() {
    print("Player number \(numberOfPlayers) rolled a dice at number \(board)")
    
    
}

boardSetup()
var hasWinner: Bool = false
var playerPosition = 0

print("Start running")
var round:  Int = 0


while (hasWinner == false) {
    round =  round + 1
    print("Round\(round)")
    for index in 1...numberOfPlayers {
        if playerPosition >= 100 {
            hasWinner = true
        } else {
            playerPosition = playerPosition + rollDice()
            
            if playerPosition < 100 {
                var bonusMove = board[playerPosition]
                print("Checking bonus for \(playerPosition): Should add \(bonusMove)")
            }
            
            print("Player \(index) is now at \(playerPosition)")
            
        }
    }
    
    // Check for winners
    //Repeat until winner is found
    
    
}
//: [Next](@next)

